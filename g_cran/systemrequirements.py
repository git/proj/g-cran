#Copyright[yyyy] [name of copyright owner]
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

import re

def parse_system_requirements(req):
    nonalpha=r'([^a-zA-Z]|$|^)'
    regex_map={
        r'Gnu Scientific Library':'sci-libs/gsl',
        r'cairo':'x11-libs/cairo',
        r'ActiveTcl':'dev-lang/tcl',
        r'(?i)java':'virtual/jre',
        r'(?i)'+nonalpha+r'libgd'+nonalpha:'media-libs/gd',
        r'boost':'dev-libs/boost',
        r'(?i)berkeleydb':'dev-perl/BerkeleyDB',
        nonalpha+r'curl'+nonalpha:'net-misc/curl',
        r'(?i)'+nonalpha+r'atk'+nonalpha:'dev-libs/atk',
        r'(?i)'+nonalpha+r'pango'+nonalpha:'x11-libs/pango',
        r'(?i)netcdf':'sci-libs/netcdf',
        r'(?i)udunits':'sci-libs/udunits',
        r'(?i)odbc':'dev-db/libiodbc',
        r'(?i)protocol buffer':'dev-libs/protobuf',
        r'(?i)'+nonalpha+'python'+nonalpha:'dev-lang/python',
        r'(?i)'+nonalpha+'quantlib'+nonalpha:'dev-libs/quantlib',
        r'(?i)'+nonalpha+'mpfr'+nonalpha:'dev-libs/mpfr',
        r'(?i)'+nonalpha+'postgresql'+nonalpha:'dev-db/postgresql',
        r'(?i)postgresql jdbc':'dev-java/jdbc-postgresql',
        r'(?i)'+nonalpha+'yacas'+nonalpha:'sci-mathematics/yacas',
        r'(?i)'+nonalpha+r'nvcc'+nonalpha:'dev-util/nvidia-cuda-sdk',
        r'(?i)'+nonalpha+r'(lib)?xml2'+nonalpha:'dev-libs/libxml2',
        r'(?i)'+nonalpha+r'(lib)?jpeg'+nonalpha:'media-libs/jpeg',
        r'(?i)'+nonalpha+r'fftw'+nonalpha:'sci-libs/fftw',
        r'(?i)'+nonalpha+r'gmp'+nonalpha:'dev-libs/gmp',
        r'(?i)'+nonalpha+r'gmt'+nonalpha:'sci-geosciences/gmt',
        r'(?i)nvidia.*toolkit':'dev-util/nvidia-cuda-toolkit',
        r'(?i)'+nonalpha+r'ghostscript'+nonalpha:'app-text/ghostscript-gpl',
        r'(?i)'+nonalpha+r'graphviz'+nonalpha:'dev-perl/GraphViz',
        r'(?i)'+nonalpha+r'(lib)?png'+nonalpha:'media-libs/libpng',
        r'(?i)'+nonalpha+r'proj'+nonalpha:'sci-libs/proj',
        r'(?i)'+nonalpha+r'(lib)?itpp'+nonalpha:'sci-libs/itpp',
        r'(?i)'+nonalpha+r'bwidget'+nonalpha:'dev-tcltk/bwidget',
        r'(?i)'+nonalpha+r'tktable'+nonalpha:'dev-tcltk/tktable',
        r'(?i)'+nonalpha+r'gdal'+nonalpha:'sci-libs/gdal',
        r'(?i)'+nonalpha+r'ggobi'+nonalpha:'sci-visualization/ggobi',
        r'(?i)'+nonalpha+r'opengl'+nonalpha:'virtual/opengl',
        r'(?i)'+nonalpha+r'glu library'+nonalpha:'virtual/glu',
        r'(?i)'+nonalpha+r'zlib'+nonalpha:'sys-libs/zlib',
        r'(?i)'+nonalpha+r'xclip'+nonalpha:'x11-misc/xclip',
        r'(Bwidget|Tktable|Tk[a-zA-Z]+).*[^a-zA-Z]Img[^a-zA-Z]':'dev-tcltk/tkimg',
        r'(?i)'+nonalpha+r'(lib)?tiff'+nonalpha:'media-libs/tiff',
        r'(?i)'+nonalpha+r'grass'+nonalpha:'sci-geosciences/grass',
        nonalpha+r'MPI[0-9]?'+nonalpha:'virtual/mpi',
        r'(?i)'+nonalpha+r'tcl'+nonalpha:'dev-lang/tcl',
        r'(?i)'+nonalpha+r'tk'+nonalpha:'dev-lang/tk',
        nonalpha+r'PARI'+nonalpha:'sci-mathematics/pari',
        r'(?i)'+nonalpha+r'gtk\+'+nonalpha:'x11-libs/gtk+',
        r'(?i)'+nonalpha+r'gsl'+nonalpha:'sci-libs/gsl',
        r'(?i)'+nonalpha+r'glib'+nonalpha:'dev-libs/glib',
        r'(?i)'+nonalpha+r'mpich2'+nonalpha:'sys-cluster/mpich2',
        r'(?i)'+nonalpha+r'openmpi'+nonalpha:'sys-cluster/openmpi',
        r'(?i)'+nonalpha+r'blas'+nonalpha:'virtual/blas',
        r'(?i)'+nonalpha+r'(lib)?xerces-c'+nonalpha:'dev-libs/xerces-c',
        r'(?i)'+nonalpha+r'freetype'+nonalpha:'media-libs/freetype',
        #r'(?i)'+nonalpha+r''+nonalpha:'',
        #r'(?i)'+nonalpha+r''+nonalpha:'',
        #r'(?i)'+nonalpha+r''+nonalpha:'',
        #r'(?i)'+nonalpha+r''+nonalpha:'',
    }
    deps=[]
    for key, value in regex_map.iteritems():
        if re.search(key,req)!=None: #found regex, output dependency
            deps.append(value)
    return deps

