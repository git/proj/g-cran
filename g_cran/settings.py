#Copyright[yyyy] [name of copyright owner]
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

#what phases can we have in our ebuild?
pms_phases=['pkg_pretend','pkg_setup','src_unpack','src_prepare','src_configure','src_compile',
    'src_test','src_install','pkg_preinst','pkg_postinst','pkg_prerm','pkg_postrm','pkg_config','pkg_info','pkg_nofetch']
actions_wanted=['usage','sync','list-categories','list-packages','package']+pms_phases
REPO_MYDIR=".g-cran" #hidden directory to save our data in a local repo
