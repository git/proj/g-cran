#Copyright[yyyy] [name of copyright owner]
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

#we use this class to easily read the PACKAGES file
#an empty line denotes a new package, but readline() usually
#doesn't return empty lines
class EmptyLinesFile:
    def __init__(self,myfile):
        self.file=myfile
        self.eof=False
        self.lines=self.generate_lines()
    def generate_lines(self,size=-1):
        for line in self.file:
            yield line
        self.eof=True
    def readline(self):
        try:
            return self.lines.next()
        except StopIteration:
            return ''
