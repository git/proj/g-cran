#!/usr/bin/python
#Copyright[yyyy] [name of copyright owner]
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

import sys, os
from ebuild import *
from cran_read import *
from filetricks import *
from settings import *

__doc__="Usage: "+sys.argv[0]+" <local repository directory> <action> [<action arguments>...]"

#sync a local repository's PACKAGES file
def action_sync(repo_location,remote_uri):
    import StringIO, urllib
    if not os.path.isdir(os.path.join(repo_location, REPO_MYDIR)):
        os.mkdir(os.path.join(repo_location,REPO_MYDIR))
    packages_filename=os.path.join(repo_location, REPO_MYDIR, 'PACKAGES')
    packages_rds_filename=os.path.join(repo_location, REPO_MYDIR, 'packages.rds')
    try:
        #we first try to get a serialized full database... this works for CRAN
        urllib.urlretrieve(remote_uri+'/web/packages/packages.rds',packages_rds_filename)
        R_script=os.path.join(os.path.dirname(__file__),'convert_packages_rds.R')
        returncode=os.system('R --quiet --file='+R_script+' --args '+packages_rds_filename+' '+packages_filename)
        if returncode:
            raise RuntimeError('Could not convert packages.rds')
    except:
        urllib.urlretrieve(remote_uri+'/src/contrib/PACKAGES',packages_filename)
    repo_file=open(os.path.join(repo_location,REPO_MYDIR,'remote_uri'),'w')
    repo_file.write(remote_uri)
    repo_file.close()

    import rfc822
    packages_file=open(packages_filename,"r")
    file_parts=EmptyLinesFile(packages_file)
    database_dir=os.path.join(repo_location,REPO_MYDIR,'DESCRIPTION')
    try:
        os.makedirs(database_dir)
    except:
        pass
    #read PACKAGES file
    while not file_parts.eof:
        current_package=''
        while True:
            line=file_parts.readline()
            if len(line.strip()): #nonempty line, add to current_package
                current_package=current_package+line
            else:
                break
        rfc822_file=StringIO.StringIO(current_package)
        cran_package=dict(rfc822.Message(rfc822_file).items()) #read part of PACKAGES file
        if len(cran_package):
            pms_package=pmsify_package_data(cran_package,remote_uri) #we need to know the PMS name
            #now add the package to the database
            filename=os.path.join(database_dir,pms_package.ebuild_vars['pn'])
            description_file=open(filename,'w')
            description_file.write(current_package)
            description_file.close()
    return 0

#list categories in this repository
def list_categories(repo_location):
    print "dev-R"
    return 0

#idem ditto
def list_packages(repo_location):
    packages=read_packages(os.path.join(repo_location,REPO_MYDIR,'PACKAGES'),repo_location)
    for package in packages:
        print 'dev-R/'+package.ebuild_vars['pn'],package.ebuild_vars['pv']
    return 0

#list package details, in PMS's format
def action_package(repo_location,package_name):
    import phases
    defined_phases=[]
    package=find_package(repo_location,package_name[package_name.find('/')+1:])
    #output data
    for key,value in package.ebuild_vars.iteritems():
        if key=='pn' or key=='pv': #readonly vars, we cannot set these in ebuilds
            pass
        elif isinstance(value,str): #if string
            print key.upper()+'='+value.replace('\n','')
        elif isinstance(value,list) and key=='license':
            if len(value)>1:
                print "LICENSE=|| ( "+' '.join(value)+' )'
            else:
                print "LICENSE="+' '.join(value)
        elif isinstance(value,list): #list, concat items
            print key.upper()+'='+' '.join(value).replace('\n','')
    for pms_func in pms_phases:
        if hasattr(phases,pms_func):
            defined_phases.append(pms_func)
    print 'GCOMMON_PHASES='+' '.join(defined_phases)
    return 0

def action_generate_metadata(repo_location):
    #todo, nothing written yet.
    return 0

def usage():
    print __doc__
    return 0

def main():
    import phases
    arguments=sys.argv[1:]
    #print options, arguments
    if len(arguments)<2: #we need at least a local repository location and an action
        usage()
        sys.exit(0)
    action=arguments[1]
    repo_location=os.path.abspath(arguments[0])
    if action=='sync':
        if len(arguments)<3:
            print "The 'sync' action takes the following parameters:"
            print " * remote_repository_uri"
            sys.exit(1)
        remote_repo=arguments[2]
        return action_sync(repo_location,remote_repo)
    elif action=='generate-metadata':
        return action_generate_metadata(repo_location)
    elif action=='list-categories':
        return list_categories(repo_location)
    elif action=='list-packages':
        return list_packages(repo_location)
    elif action=='package':
        if len(arguments)<3:
            print "The 'package' action takes the following parameters:"
            print " * category/package_name"
            print " * [version]"
            sys.exit(1)
        package_name=arguments[2]
        return action_package(repo_location,package_name)
    elif action=='usage':
        return usage()
    elif action in pms_phases and hasattr(phases,action):
        return getattr(phases,action)(os.environ,repo_location)
    elif action in actions_wanted:
        raise NotImplementedError
    else:
        return usage()

if __name__ == "__main__":
    sys.exit(main())
    

