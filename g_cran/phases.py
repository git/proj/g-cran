#Copyright[yyyy] [name of copyright owner]
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

import os,shutil
import cran_read

R_PLATFORM="ignoreplatform"

def verbose_system(command):
    print command
    return os.system(command)

#no need to unpack, see src_compile
def pkg_unpack(env,local_repository):
    return 0

#This function actually creates a binary package (a tarball) from the source package
#this way, we cleanly split up R package installing into a compile and install phase
def src_compile(env,local_repository):
    os.putenv('R_PLATFORM',R_PLATFORM) #force predictable package name
    os.chdir(env['WORKDIR'])
    tarball=os.path.join(env['DISTDIR'],env['A'])

    #tmp_target is a dummy directory to make R CMD INSTALL function nicely
    tmp_target=os.path.join(env['WORKDIR'],'tmp_install')
    os.makedirs(tmp_target)
    returnval=verbose_system("R CMD INSTALL --build "+tarball+" -l "+tmp_target)
    if returnval:
        raise RuntimeError("R build failed")
    return 0

def src_install(env,local_repository):
    package=cran_read.find_package(local_repository,env['PN'])
    os.putenv('R_PLATFORM',R_PLATFORM)

    #figure out the name of the tarball we generated in src_compile
    tarname=env['WORKDIR']+'/'+package.cran_data['package']+'_'+package.cran_data['version']+'_R_'+R_PLATFORM+'.tar.gz' #assume always gzip

    #figure out target install dir
    r_home=os.getenv('R_HOME')
    if len(r_home)==0: #R home isn't set, try to read /etc/env.d/99R
        envfile=open('/etc/env.d/99R','r')
        for line in envfile:
            if line[:len('R_HOME')]=='R_HOME':
                r_home=line[line.find('=')+1:].strip()
                break
        else:
            raise RuntimeError("Could not deduce R_HOME")
    r_library=env['D']+r_home+"/library"
    if not os.path.exists(r_library):
        os.makedirs(r_library) #note this is all just in env['D']
    #install the binary package
    returnval=verbose_system("R CMD INSTALL --debug "+tarname+" -l "+r_library)
    if returnval:
        raise RuntimeError("R install failed")
    
    try: #should be installed by R already, so remove from image
        os.remove(os.path.join(r_library,'R.css'))
    except:
        pass
    
    #todo install non-HTML help
    doc_dir=env['D']+'/usr/share/doc/'+env['PF']
    if 'doc' in package.ebuild_vars['iuse'] and 'doc' in env['USE']:
        os.makedirs(doc_dir)
        os.rename(os.path.join(r_library,package.cran_data['package'],'html'),doc_dir)
        #not implemented
    else:
        shutil.rmtree(os.path.join(r_library,package.cran_data['package'],'html'))
    return 0


