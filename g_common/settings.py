#Copyright[yyyy] [name of copyright owner]
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

PMS_PHASES=['pkg_pretend','pkg_setup','src_unpack','src_prepare','src_configure','src_compile',
    'src_test','src_install','pkg_preinst','pkg_postinst','pkg_prerm','pkg_postrm','pkg_config','pkg_info','pkg_nofetch']
GLOBAL_CONF_DIR='/usr/share/g-common/'
DRIVER_DIR='drivers/'
COMMON_EBUILD_FILE=GLOBAL_CONF_DIR+'common.ebuild'
MYDIR='.g-common'

