#Copyright[yyyy] [name of copyright owner]
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

# $Header: $

#
# Original Author: Auke Booij
# Purpose: g-common compatibility layer for package managers without native
# support for g-common-capable non-ebuild repository drivers
#

this_dirname=$(/bin/dirname ${BASH_ARGV[0]})
this_repo="${this_dirname}/../../"

#discover category, package name and version
cat_pn_pvr=$(echo $this_file|/bin/sed -r 's$.+?/([A-Za-z0-9+_.-]+)/([A-Za-z0-9+_-]+)/\2-([0-9]+(\.[0-9]+)*[a-z]?((_alpha|_beta|_pre|_rc|_p)[0-9]*)*(-r[0-9]*)?)\.ebuild$\1/\2 \3$')

#this reads in metadata from g-cran
{ while read -r line
do
	export "$line"
done
} < <(/usr/bin/g-common $this_repo package $cat_pn_pvr || die);

for phase in $GCOMMON_PHASES
do
	 eval "$(printf '%q() { exec_phase %q; }' "$phase" "$phase")"
done

function exec_phase() {
	/usr/bin/g-common $this_repo $1 || die
}

