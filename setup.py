#Copyright[yyyy] [name of copyright owner]
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

#!/usr/bin/env python

from distutils.core import setup

setup(name='G-CRAN',
      version='1',
      description='CRAN-style package installer',
      author='Auke Booij (tulcod)',
      author_email='auke@tulcod.com',
      url='http://git.overlays.gentoo.org/gitweb/?p=proj/g-cran.git ',
      packages=['g_cran','g_common'],
      scripts=['bin/g-cran','bin/g-common'],
      package_data={'g_cran':['convert_packages_rds.R']},
      data_files=[
          #('/etc/g-common/',['g_common/']),
          ('/usr/share/g-common/drivers/',['g_cran/cran.cfg']),
          ('/usr/share/g-common/',['g_common/common.ebuild']),
      ]
     )
